<?xml version="1.0" encoding="utf-8"?>
<audio-profiles>
  <profiles>
    <profile id="faac-aac">
      <name>MP4/AAC</name>
      <_description>A codec designed to be the successor of the MP3 format, providing greater sound quality and transparency than MP3 files coded at the same bit rate.</_description>
      <gst-plugin-name>faac</gst-plugin-name>
      <output-file-extension>m4a</output-file-extension>
      <mimetype>audio/x-m4a</mimetype>
      <mimetype>audio/mp4</mimetype>
      <pipeline>
        <process id="gstreamer-audio">faac</process>
        <process id="multiplexer">
          <value value="ffmux_mp4">...</value>
		    </process>
        <variable id="bitrate" type="combo">
          <name>Bitrate</name>
          <variable-name>bitrate</variable-name>
          <default-value>128000</default-value>
          <possible-values>
            <value value="8000">8 Kbps</value>
            <value value="16000">16 Kbps</value>
            <value value="24000">24 Kbps</value>
            <value value="32000">32 Kbps</value>
            <value value="40000">40 Kbps</value>
            <value value="48000">48 Kbps</value>
            <value value="56000">56 Kbps</value>
            <value value="64000">64 Kbps</value>
            <value value="80000">80 Kbps</value>
            <value value="96000">96 Kbps</value>
            <value value="112000">112 Kbps</value>
            <value value="128000">128 Kbps</value>
            <value value="160000">160 Kbps</value>
            <value value="192000">192 Kbps</value>
            <value value="224000">224 Kbps</value>
            <value value="256000">256 Kbps</value>
            <value value="320000">320 Kbps</value>
          </possible-values>
        </variable>
       <variable id="profile" type="combo">
          <name>Profile</name>
          <_description>AAC takes a modular approach to encoding. There are four default profiles using different tools: (LC) -> the simplest and most widely used and supported; (MAIN) -> like the LC profile, with the addition of backwards prediction; (SRS) -> a.k.a. Scalable Sample Rate (MPEG-4 AAC-SSR); (LTP) -> an improvement of the MAIN profile using a forward predictor with lower computational complexity.</_description>
          <variable-name>profile</variable-name>
          <default-value>1</default-value>
          <possible-values>
            <value value="1">Main (MAIN)</value>
            <value value="2">Low complexity (LC)</value>
            <value value="3">Scalable sampling rate (SSR)</value>
            <value value="4">Long term prediction (LTP)</value>
          </possible-values>
        </variable>
       <variable id="tns" type="check">
          <name>Temporal noise shaping</name>
          <_description>Conventional transform coding schemes often encounter problems with signals that vary heavily over time, especially speech signals. Temporal noise shaping can be viewed as a postprocessing step which goal is to overcome this limitation.</_description>
          <variable-name>tns</variable-name>
          <default-value>false</default-value>
        </variable>
        <variable id="outputformat" type="combo">
          <name>Output format</name>
          <variable-name>outputformat</variable-name>
          <default-value>1</default-value>
          <possible-values>
            <_value value="1">ADTS headers</_value>
            <_value value="0">Raw AAC</_value>
          </possible-values>
        </variable>
      </pipeline>
    </profile>
  </profiles>
</audio-profiles>
