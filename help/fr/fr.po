# French translation for gnac.
# Copyright (C) 2012 gnac's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnac package.
#
# Bruno Brouard <annoa.b@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: gnac master\n"
"POT-Creation-Date: 2012-02-11 15:33+0000\n"
"PO-Revision-Date: 2012-02-12 19:17+0100\n"
"Last-Translator: Bruno Brouard <annoa.b@gmail.com>\n"
"Language-Team: French <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"Language: fr\n"
"X-Generator: Lokalize 1.2\n"

#: C/license.page:8(desc)
msgid "Legal information."
msgstr "Informations légales."

#: C/license.page:11(title)
msgid "License"
msgstr "Licence"

#: C/license.page:12(p)
msgid ""
"This work is distributed under a CreativeCommons Attribution-Share Alike 3.0 "
"Unported license."
msgstr ""
"Cette œuvre est distribuée sous licence Creative Commons Paternité-Partage "
"des Conditions Initiales à l'Identique 3.0 Unported."

#: C/license.page:18(p)
msgid "You are free:"
msgstr "Vous êtes libre :"

#: C/license.page:23(em)
msgid "To share"
msgstr "de partager,"

#: C/license.page:24(p)
msgid "To copy, distribute and transmit the work."
msgstr "de reproduire, distribuer et communiquer cette création,"

#: C/license.page:27(em)
msgid "To remix"
msgstr "de modifier,"

#: C/license.page:28(p)
msgid "To adapt the work."
msgstr "d'adapter cette création."

#: C/license.page:31(p)
msgid "Under the following conditions:"
msgstr "Selon les conditions suivantes :"

#: C/license.page:36(em)
msgid "Attribution"
msgstr "Paternité"

#: C/license.page:37(p)
msgid ""
"You must attribute the work in the manner specified by the author or "
"licensor (but not in any way that suggests that they endorse you or your use "
"of the work)."
msgstr ""
"Vous devez citer le nom de l'auteur original de la manière indiquée par "
"l'auteur de l'œuvre ou le titulaire des droits qui vous confère cette "
"autorisation (mais pas d'une manière qui suggérerait qu'ils vous soutiennent "
"ou approuvent votre utilisation de l'œuvre)."

#: C/license.page:44(em)
msgid "Share Alike"
msgstr "Partage des Conditions Initiales à l'Identique"

#: C/license.page:45(p)
msgid ""
"If you alter, transform, or build upon this work, you may distribute the "
"resulting work only under the same, similar or a compatible license."
msgstr ""
"Si vous modifiez, transformez ou adaptez cette création, vous n'avez le droit "
"de distribuer la création qui en résulte que sous un contrat identique à "
"celui-ci."

#: C/license.page:51(p)
msgid ""
"For the full text of the license, see the <link href=\"http://"
"creativecommons.org/licenses/by-sa/3.0/legalcode\">CreativeCommons website</"
"link>, or read the full <link href=\"http://creativecommons.org/licenses/by-"
"sa/3.0/\">Commons Deed</link>."
msgstr ""
"Pour prendre connaissance du texte complet de la licence, consultez le <link "
"href=\"http://creativecommons.org/licenses/by-sa/3.0/legalcode\">site Web "
"CreativeCommons</link> ou lisez in extenso le <link "
"href=\"http://creativecommons.org/licenses/by-sa/3.0/\">contrat Commons</link>"
"."

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: C/intro.page:19(None)
msgid ""
"@@image: 'figures/gnac-main-window.png'; md5=fcbf156ab09eb764adf71931c6e5cf25"
msgstr ""

#: C/intro.page:7(desc)
msgid "Introduction to <app>Gnac</app>."
msgstr "Introduction à <app>Gnac</app>."

#: C/intro.page:10(title)
msgid "Introduction"
msgstr "Introduction"

#: C/intro.page:12(p)
msgid ""
"<app>Gnac</app> is an audio converter for the GNOME desktop designed to be "
"easy to use yet powerful. <app>Gnac</app> can easily convert audio files "
"between all formats supported by GStreamer. In addition, the creation and "
"management of audio profiles have been improved allowing everyone to easily "
"create or customize their own profiles."
msgstr ""
"<app>Gnac</app> est un convertisseur audio pour le bureau GNOME conçu pour "
"être "
"puissant et facile à utiliser. <app>Gnac</app> peut facilement convertir les "
"fichiers audio "
"entre les différents formats pris en charge par GStreamer. De plus, la "
"création et la gestion "
"de profils audio a été améliorée pour permettre à tout le monde de facilement "
"créer et personnaliser "
"ses propres profils."

#: C/intro.page:17(title)
msgid "<gui>Gnac</gui> window"
msgstr "fenêtre de <gui>Gnac</gui>"

#: C/intro.page:18(desc)
msgid "<app>Gnac</app> main window"
msgstr "fenêtre principale de <gui>Gnac</gui>"

#: C/intro.page:20(p)
msgid "<app>Gnac</app> main window."
msgstr "fenêtre principale de <gui>Gnac</gui>."

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: C/index.page:11(None)
msgid "@@image: 'figures/gnac-logo.png'; md5=cdba92665c0de719e421c733090f6977"
msgstr "@@image: 'figures/gnac-logo.png'; md5=cdba92665c0de719e421c733090f6977"

#: C/index.page:6(title) C/index.page:7(title)
msgid "Gnac"
msgstr "Gnac"

#: C/index.page:10(title)
msgid ""
"<media type=\"image\" mime=\"image/png\" src=\"figures/gnac-logo.png\"> Gnac "
"logo </media> Gnac"
msgstr ""
"<media type=\"image\" mime=\"image/png\" src=\"figures/gnac-logo.png\">logo "
"de Gnac</media> Gnac"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: C/index.page:0(None)
msgid "translator-credits"
msgstr "Bruno Brouard <annoa.b@gmail.com>, 2012"

