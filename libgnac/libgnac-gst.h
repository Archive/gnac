/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __LIBGNAC_GST_H__
#define __LIBGNAC_GST_H__

#include <gio/gio.h>
#include <glib.h>
#include <glib-object.h>
#include <gst/gst.h>
#include <gst/pbutils/pbutils.h>

#include "libgnac-converter.h"
#include "libgnac-media-item.h"
#include "libgnac-profile.h"

G_BEGIN_DECLS

void
libgnac_gst_run(LibgnacMediaItem  *item, 
                GError           **error);

void
libgnac_gst_stop(LibgnacMediaItem  *item, 
                 GError           **error);

void
libgnac_gst_pause(LibgnacMediaItem  *item, 
                  GError           **error);

void
libgnac_gst_build_pipeline(LibgnacMediaItem  *item,
                           GError           **error);

void
libgnac_gst_clean_pipeline(LibgnacMediaItem *item);

void
libgnac_gst_newpad_cb(GstElement *decodebin,
                      GstPad     *pad,
                      gpointer    data);

G_END_DECLS

#endif /* __LIBGNAC_GST_H__ */
