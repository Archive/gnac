/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __LIBGNAC_GST_UTILS_H__
#define __LIBGNAC_GST_UTILS_H__

#include <glib.h>
#include <glib-object.h>
#include <gst/gst.h>

G_BEGIN_DECLS

GstElement *
libgnac_gstu_make_pipeline_element(GstElement   *bin,
                                   const gchar  *element,
                                   const gchar  *name,
                                   GError      **error);

GstElement *
libgnac_gstu_pipeline_new(GError **error);

gboolean
libgnac_gstu_bin_add(GstElement  *bin,
                     GstElement  *elem,
                     GError     **error);

gboolean
libgnac_gstu_element_link(GstElement  *src,
                          GstElement  *dst,
                          GError     **error);

GstPadLinkReturn
libgnac_gstu_pad_link(GstPad  *src,
                      GstPad  *sink,
                      GError **error);

gboolean
libgnac_gstu_get_compatible_pad(GstElement  *element,
                                GstPad      *pad,
                                GstCaps     *caps,
                                const gchar *type);

G_END_DECLS

#endif /* __LIBGNAC_GST_UTILS_H__ */
