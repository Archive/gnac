/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef __LIBGNAC_CONVERTER_H__
#define __LIBGNAC_CONVERTER_H__

#include <gio/gio.h>
#include <glib.h>
#include <glib-object.h>
#include <gst/gst.h>
#include <gst/pbutils/pbutils.h>

#include "libgnac-media-item.h"
#include "libgnac-metadata.h"
#include "libgnac-metadata-tags.h"
#include "libgnac-output.h"

#define PROGRESS_TIMEOUT 250

enum {
  OVERWRITE,
  PROGRESS,
  FILE_ADDED,
  FILE_REMOVED,
  FILE_STARTED,
  FILE_COMPLETED,
  FILES_CLEARED,
  STARTED,
  STOPPED,
  COMPLETION,
  PLUGIN_INSTALL,
  ERROR,
  LAST_SIGNAL
};

extern guint signals[LAST_SIGNAL];

G_BEGIN_DECLS

#define LIBGNAC_TYPE_CONVERTER (libgnac_converter_get_type())
#define LIBGNAC_CONVERTER(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), \
    LIBGNAC_TYPE_CONVERTER, LibgnacConverter))
#define LIBGNAC_IS_CONVERTER(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), \
    LIBGNAC_TYPE_CONVERTER))
#define LIBGNAC_CONVERTER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), \
    LIBGNAC_TYPE_CONVERTER, LibgnacConverterClass))
#define LIBGNAC_IS_CONVERTER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), \
    LIBGNAC_TYPE_CONVERTER))
#define LIBGNAC_CONVERTER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), \
    LIBGNAC_TYPE_CONVERTER, LibgnacConverterClass))

typedef struct LibgnacConverterPrivate LibgnacConverterPrivate;

typedef struct
{
  GObject parent_instance;

  /* private */
  LibgnacConverterPrivate *priv;

} LibgnacConverter;


typedef struct
{
  GObjectClass parent_class;

  /* class members */

  /* signals */
  gboolean (*overwrite) (LibgnacConverter *converter, gpointer file);
  void (*progress) (LibgnacConverter *converter, gfloat fraction);
  void (*file_added) (LibgnacConverter *converter, gchar *uri);
  void (*file_removed) (LibgnacConverter *converter, gchar *uri);
  void (*file_started) (LibgnacConverter *converter, gchar *input);
  void (*file_completed) (LibgnacConverter *converter, gchar *uri);
  void (*files_cleared) (LibgnacConverter *converter);
  void (*started) (LibgnacConverter *converter);
  void (*stopped) (LibgnacConverter *converter);
  void (*completion) (LibgnacConverter *converter);
  void (*plugin_install) (LibgnacConverter *converter, gboolean done);
  void (*error) (LibgnacConverter *converter, gchar *uri, gchar *msg, GError *error);

} LibgnacConverterClass;


GType 
libgnac_converter_get_type(void);

LibgnacOutputConfig *
libgnac_converter_get_config(LibgnacConverter *self);

void
libgnac_converter_free_config(LibgnacOutputConfig *config);

GObject *
libgnac_converter_new(LibgnacMetadata *mdata);

void
libgnac_converter_add(LibgnacConverter  *self,
                      GFile             *file,
                      GError           **error);

void
libgnac_converter_remove(LibgnacConverter  *self,
                         GFile             *file,
                         GError           **error);

void
libgnac_converter_clear(LibgnacConverter *self);

void
libgnac_converter_start(LibgnacConverter  *self,
                        GError           **error);

void
libgnac_converter_stop(LibgnacConverter  *self,
                       GError           **error);

void
libgnac_converter_pause(LibgnacConverter  *self,
                        GError           **error);

void
libgnac_converter_resume(LibgnacConverter  *self,
                         GError           **error);

void
libgnac_converter_emit_plugin_install(LibgnacConverter *converter);

void
libgnac_converter_emit_plugin_installed(LibgnacConverter *converter);

/* Callbacks */

void
libgnac_converter_eos_cb(GstBus     *bus, 
                         GstMessage *message, 
                         gpointer    user_data);
void
libgnac_converter_error_cb(GstBus     *bus, 
                           GstMessage *message, 
                           gpointer    user_data);

void
libgnac_converter_missing_plugin_result_cb(GstInstallPluginsReturn result,
                                           gpointer                user_data);

gboolean
libgnac_converter_percentage_cb(LibgnacConverter *converter);

G_END_DECLS

#endif /* __LIBGNAC_CONVERTER_H__ */
