/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>

#include "libgnac-debug.h"
#include "libgnac-error.h"
#include "libgnac-gst-utils.h"


GstElement *
libgnac_gstu_make_pipeline_element(GstElement   *bin,
                                   const gchar  *element,
                                   const gchar  *name,
                                   GError      **error)
{
  GstElement *elem = gst_element_factory_make(element, name);
  if (!elem) {
    libgnac_debug("Failed to create %s element", element);
    g_set_error(error, LIBGNAC_ERROR, LIBGNAC_ERROR_MISSING_PLUGIN,
        "%s", element);
    return NULL;
  }

  if (!gst_bin_add(GST_BIN(bin), elem)) {
    libgnac_debug("Failed to add %s element to the bin", element);
    g_set_error(error, LIBGNAC_ERROR, LIBGNAC_ERROR_PIPELINE_CREATION,
        _("Failed to add %s element"), element);
    return NULL;
  }

  return elem;
}


GstElement *
libgnac_gstu_pipeline_new(GError **error)
{
  GstElement *pipeline = gst_pipeline_new("pipeline");
  if (!pipeline) {
    libgnac_warning("Pipeline creation failed");
    g_set_error(error, LIBGNAC_ERROR, LIBGNAC_ERROR_PIPELINE_CREATION,
        _("Unable to create pipeline"));
    return NULL;
  }

  return pipeline;
}


gboolean
libgnac_gstu_bin_add(GstElement  *bin,
                     GstElement  *elem,
                     GError     **error)
{
  if (!gst_bin_add(GST_BIN(bin), elem)) {
    gchar *name = gst_element_get_name(elem);
    libgnac_debug("Failed to add %s element to the bin", name);
    g_set_error(error, LIBGNAC_ERROR, LIBGNAC_ERROR_PIPELINE_CREATION,
        _("Failed to add %s element"), name);
    g_free(name);
    return FALSE;
  }

  return TRUE;
}


gboolean
libgnac_gstu_element_link(GstElement  *src,
                          GstElement  *dst,
                          GError     **error)
{
  gboolean ret = gst_element_link(src, dst);
  if (!ret) {
    libgnac_debug("Failed to link element %s to %s",
        gst_element_get_name(src), gst_element_get_name(dst));
    g_set_error(error, LIBGNAC_ERROR, LIBGNAC_ERROR_PIPELINE_CREATION,
        _("Unable to link element %s to %s"),
        gst_element_get_name(src), gst_element_get_name(dst));
  }

  return ret;
}


GstPadLinkReturn
libgnac_gstu_pad_link(GstPad  *src,
                      GstPad  *sink,
                      GError **error)
{
  GstPadLinkReturn ret = gst_pad_link(src, sink);
  if (ret != GST_PAD_LINK_OK) {
    libgnac_debug("Failed to link pad %s to %s",
        gst_pad_get_name(src), gst_pad_get_name(sink));
    g_set_error(error, LIBGNAC_ERROR, LIBGNAC_ERROR_PIPELINE_CREATION,
        _("Unable to link pad %s to %s"),
        gst_pad_get_name(src), gst_pad_get_name(sink));
  }

  return ret;
}


gboolean
libgnac_gstu_get_compatible_pad(GstElement  *element,
                                GstPad      *pad,
                                GstCaps     *caps,
                                const gchar *type)
{
  gchar *pad_name = gst_pad_get_name(pad);

  GstPad *sink_pad = gst_element_get_compatible_pad(element, pad, caps);
  if (!sink_pad) {
    gchar *caps_str = gst_caps_to_string(caps);
    libgnac_debug("Unable to find a compatible %s pad "
        "(sink_pad = %s, caps = %s\n)", type, pad_name, caps_str);
    g_free(pad_name);
    g_free(caps_str);
    gst_caps_unref(caps);
    return FALSE;
  }

  gchar *sink_pad_name = gst_pad_get_name(sink_pad);

  libgnac_debug("%s: %s -> %s", type, pad_name, sink_pad_name);

  if (GST_PAD_IS_LINKED(sink_pad)) {
    libgnac_debug("%s pad %s is already linked", type, sink_pad_name);
    g_free(pad_name);
    g_free(sink_pad_name);
    gst_object_unref(sink_pad);
    gst_caps_unref(caps);
    return FALSE;
  }

  g_return_val_if_fail(
      libgnac_gstu_pad_link(pad, sink_pad, NULL) == GST_PAD_LINK_OK,
      FALSE);

  g_free(pad_name);
  g_free(sink_pad_name);
  gst_object_unref(sink_pad);

  return TRUE;
}
