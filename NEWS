Gnac -- History of visible changes.

Copyright (C) 2007 - 2012, Gnac
See the end for copying conditions.

Version 0.2.4.1 (May 2012)

* Added support for ASX playlists

Version 0.2.4 (February 2012)

* Ported GTK2 to GTK3
* New translations: British English, Lithuanian, Polish, Telugu

Version 0.2.3 (May 2011)

* Extract and convert audio from video
* Notification on conversion completed
* Migration to GSettings

Version 0.2.2 (June 2010)

* File chooser specially designed to ease the use of file filters
* Display VBR bitrate
* Libunique support: allow only one instance of Gnac
* Verbose and debug options
* Many new translations (thank you very much to our translators!)

Version 0.2.1 (August 2009)

* Custom rename patterns and folder hierarchy,
* Option to strip special characters in filenames,
* Possibility to copy an existing profile,
* Improvements in the graphical user interface and the user-friendliness.

Version 0.2 (April 2009)

* New logo (thanks to Cristian Grada),
* Easy profiles support,
* Redesigned properties window,
* New translations: cs, he, it, ro, sv,
* More tags displayed (property window remodeled),
* New profile manager.

Version 0.1.1 (August 2008)

* Progressbar improved,
* More information about files displayed,
* Code improved to make gnac more stable.

Version 0.1 (July 2008)

* First release,
* Handle all formats supported by GStreamer,
* Handle playlists (*.m3u, *.pls, *.xspf),
* Tags conserved after the conversion.

---------------------------------------------
Copying information:

Copyright (C) 2007 - 2012, Gnac

  Permission is granted to anyone to make or distribute verbatim copies
  of this document as received, in any medium, provided that the
  copyright notice and this permission notice are preserved,
  thus giving the recipient permission to redistribute in turn.

  Permission is granted to distribute modified versions
  of this document, or of portions of it,
  under the above conditions, provided also that they
  carry prominent notices stating who last changed them.
