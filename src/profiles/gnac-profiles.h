/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_PROFILES_H
#define GNAC_PROFILES_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

void
gnac_profiles_init(void);

void
gnac_profiles_populate_combo(void);

const gchar *
gnac_profiles_get_pipeline(void);

const gchar *
gnac_profiles_get_extension(void);

const gchar *
gnac_profiles_get_name(void);

void
gnac_profiles_on_combo_profile_changed(GtkWidget *widget,
                                       gpointer   data);

G_END_DECLS

#endif /* GNAC_PROFILES_H */
