/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <glib/gstdio.h>

#include "../gnac-profiles-properties.h"
#include "gnac-profiles-wav.h"


BasicFormatInfo wav_bfi = {
  PKGDATADIR "/profiles/gnac-profiles-wav.xml",
  NULL,
  PKGDATADIR "/profiles/wav.xml",
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL
};


static const gchar *
gnac_profiles_wav_init(void)
{
  gnac_profiles_default_init(&wav_bfi);
  
  gnac_profiles_xml_engine_free_doc_xpath(wav_bfi.doc);
  wav_bfi.doc = NULL;

  return wav_bfi.format_id;
}


static void
gnac_profiles_wav_generate_pipeline(void)
{
  gchar *pipeline = gnac_profiles_default_generate_pipeline(&wav_bfi);
  gnac_profiles_properties_update_textbuffer(pipeline);

  g_free(wav_bfi.pipeline);

  wav_bfi.pipeline = pipeline;
}


static void
gnac_profiles_wav_set_fields(gpointer data)
{
  /* nothing to do */
}


static gchar *
gnac_profiles_wav_get_combo_format_name(void)
{
  return gnac_profiles_default_get_combo_format_name(&wav_bfi);
}


static void
gnac_profiles_wav_free_audio_profile(gpointer data)
{
  if (!data) return;

  AudioProfileGeneric *profile = (AudioProfileGeneric *) data;
  gnac_profiles_utils_free_audio_profile_generic(profile->generic); 
}


static gpointer
gnac_profiles_wav_generate_audio_profile(GError **error)
{
  return gnac_profiles_default_generate_audio_profile(&wav_bfi);
}


static GtkWidget *
gnac_profiles_wav_get_widget(void)
{
  return gnac_profiles_default_get_properties_alignment(&wav_bfi);
}


static void
gnac_profiles_wav_save_profile(gpointer data)
{
  if (!data) return;

  AudioProfileGeneric *profile = (AudioProfileGeneric *) data;
  XMLDoc *doc = gnac_profiles_default_save_profile(profile, &wav_bfi);
  gnac_profiles_xml_engine_save_doc(doc, profile->generic->name);
  gnac_profiles_xml_engine_free_doc_xpath(doc);
}


static gpointer
gnac_profiles_wav_load_specific_properties(XMLDoc              *doc,
                                           AudioProfileGeneric *generic)
{
  return generic;
}


static void
gnac_profiles_wav_clean_up(void)
{
  gnac_profiles_default_clean_up(&wav_bfi);
  gnac_profiles_utils_free_basic_format_info(&wav_bfi);
}


static const gchar *
gnac_profiles_wav_get_plugin_name(void)
{
  return wav_bfi.format_plugin_name;
}


static const gchar *
gnac_profiles_wav_get_description(void)
{
  return wav_bfi.description;
}


FormatModuleFuncs
gnac_profiles_wav_get_funcs(void)
{
  FormatModuleFuncs funcs = {
    gnac_profiles_wav_init,
    gnac_profiles_wav_get_description,
    gnac_profiles_wav_generate_pipeline,
    gnac_profiles_wav_generate_audio_profile,
    gnac_profiles_wav_free_audio_profile,
    gnac_profiles_wav_set_fields,
    gnac_profiles_wav_get_widget,
    gnac_profiles_wav_save_profile,
    gnac_profiles_wav_load_specific_properties,
    gnac_profiles_wav_clean_up,
    NULL,
    gnac_profiles_wav_get_combo_format_name,
    gnac_profiles_wav_get_plugin_name
  };

  return funcs;
}
