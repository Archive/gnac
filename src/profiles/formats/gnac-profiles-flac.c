/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <glib/gstdio.h>

#include "../gnac-profiles-properties.h"
#include "gnac-profiles-flac.h"


typedef struct
{
  AudioProfileGeneric *generic;

  gdouble quality;
}
AudioProfileFlac;

BasicFormatInfo flac_bfi = {
  PKGDATADIR "/profiles/gnac-profiles-flac.xml",
  NULL,
  PKGDATADIR "/profiles/flac.xml",
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL
};


static const gchar *
gnac_profiles_flac_init(void)
{
  gnac_profiles_default_init(&flac_bfi);

  gnac_profiles_utils_init_widget(&flac_bfi, "slider-compression",
      "//variable[@id='compression']");
  
  gnac_profiles_xml_engine_free_doc_xpath(flac_bfi.doc);
  flac_bfi.doc = NULL;

  return flac_bfi.format_id;
}


void
gnac_profiles_flac_generate_pipeline(void)
{
  gchar *pipeline = gnac_profiles_default_generate_pipeline(&flac_bfi);
  pipeline = gnac_profiles_utils_add_properties_slider(pipeline, &flac_bfi,
      "%.0f", "slider-compression", NULL);
  gnac_profiles_properties_update_textbuffer(pipeline);

  g_free(flac_bfi.pipeline);

  flac_bfi.pipeline = pipeline;
}


static void
gnac_profiles_flac_reset_ui(void)
{
  gnac_profiles_default_reset_ui(&flac_bfi);
  gnac_profiles_utils_reset_values(&flac_bfi,
      "slider-compression", NULL);
}


static void
gnac_profiles_flac_set_fields(gpointer data)
{
  if (!data) {
    gnac_profiles_flac_reset_ui();
    return;
  }

  AudioProfileFlac *profile = (AudioProfileFlac *) data;
  gnac_profiles_utils_set_values(&flac_bfi,
      "slider-compression", profile->quality, NULL);
}


static gchar *
gnac_profiles_flac_get_combo_format_name(void)
{
  return gnac_profiles_default_get_combo_format_name(&flac_bfi);
}


static void
gnac_profiles_flac_free_audio_profile(gpointer data)
{
  if (!data) return;

  AudioProfileFlac *profile = (AudioProfileFlac *) data;
  gnac_profiles_utils_free_audio_profile_generic(profile->generic); 
  g_free(profile);
}


static gpointer
gnac_profiles_flac_generate_audio_profile(GError **error)
{
  AudioProfileGeneric *generic = gnac_profiles_default_generate_audio_profile(
      &flac_bfi);
  AudioProfileFlac *profile = g_malloc(sizeof(AudioProfileFlac));

  profile->generic = generic;

  gnac_profiles_utils_get_values_and_set(&flac_bfi,
      "slider-compression", &profile->quality, NULL);
  
  return profile;
}


static GtkWidget *
gnac_profiles_flac_get_widget(void)
{
  return gnac_profiles_default_get_properties_alignment(&flac_bfi);
}


static void
gnac_profiles_flac_save_profile(gpointer data)
{
  if (!data) return;

  AudioProfileFlac *profile = (AudioProfileFlac *) data;
  gchar *quality = gnac_profiles_utils_gdouble_to_gchararray(profile->quality);
  XMLDoc *doc = gnac_profiles_default_save_profile(profile->generic, &flac_bfi);
  gnac_profiles_xml_engine_add_values(doc,
      "quality", quality, NULL);
  gnac_profiles_xml_engine_save_doc(doc, profile->generic->name);
  gnac_profiles_xml_engine_free_doc_xpath(doc);
  g_free(quality);
}


static gpointer
gnac_profiles_flac_load_specific_properties(XMLDoc              *doc,
                                            AudioProfileGeneric *generic)
{
  AudioProfileFlac *profile = g_malloc(sizeof(AudioProfileFlac));

  profile->generic = generic;

  gchar *quality;
  gnac_profiles_utils_load_saved_profile(doc,
      "/audio-profile/format-specific/", "quality", &quality, NULL);
  
  if (quality) {
    profile->quality = gnac_profiles_utils_gchararray_to_gdouble(quality);
    g_free(quality);
  }
  
  return profile;
}


static void
gnac_profiles_flac_clean_up(void)
{
  gnac_profiles_default_clean_up(&flac_bfi);
  gnac_profiles_utils_free_values(&flac_bfi, "slider-compression", NULL);
  gnac_profiles_utils_free_basic_format_info(&flac_bfi);
}


static const gchar *
gnac_profiles_flac_get_plugin_name(void)
{
  return flac_bfi.format_plugin_name;
}


static const gchar *
gnac_profiles_flac_get_description(void)
{
  return flac_bfi.description;
}


FormatModuleFuncs
gnac_profiles_flac_get_funcs(void)
{
  FormatModuleFuncs funcs = {
    gnac_profiles_flac_init,
    gnac_profiles_flac_get_description,
    gnac_profiles_flac_generate_pipeline,
    gnac_profiles_flac_generate_audio_profile,
    gnac_profiles_flac_free_audio_profile,
    gnac_profiles_flac_set_fields,
    gnac_profiles_flac_get_widget,
    gnac_profiles_flac_save_profile,
    gnac_profiles_flac_load_specific_properties,
    gnac_profiles_flac_clean_up,
    NULL,
    gnac_profiles_flac_get_combo_format_name,
    gnac_profiles_flac_get_plugin_name
  };

  return funcs;
}
