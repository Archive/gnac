/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <glib/gstdio.h>

#include "../gnac-profiles-properties.h"
#include "gnac-profiles-aac.h"


typedef struct
{
  AudioProfileGeneric *generic;

  gchar *bitrate;
  gchar *outputformat;
  gchar *profile;
  gchar *tns;
}
AudioProfileAAC;

BasicFormatInfo aac_bfi = {
  PKGDATADIR "/profiles/gnac-profiles-aac.xml",
  NULL,
  PKGDATADIR "/profiles/aac.xml",
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL
};


static const gchar *
gnac_profiles_aac_init(void)
{
  GtkWidget *widget;
  GtkWidget *widget2;

  gnac_profiles_default_init(&aac_bfi);

  // Bitrate
  gnac_profiles_utils_init_widget(&aac_bfi, "combo-bitrate",
      "//variable[@id='bitrate']");

  // Profile
  widget = gnac_profiles_utils_init_widget(&aac_bfi, "combo-profile",
      "//variable[@id='profile']");
  widget2 = gnac_profiles_utils_get_widget(&aac_bfi, "label-profile"),
  gnac_profiles_utils_add_description_tooltip(&aac_bfi,
      "//variable[@id='profile']/description", widget, widget2, NULL);
  
  // Tns
  widget = gnac_profiles_utils_init_widget(&aac_bfi, "checkbutton-tns",
      "//variable[@id='tns']");
  gnac_profiles_utils_add_description_tooltip(&aac_bfi,
      "//variable[@id='tns']/description", widget, NULL);
  
  // Outputformat
  gnac_profiles_utils_init_widget(&aac_bfi, "combo-outputformat",
      "//variable[@id='outputformat']");

  gnac_profiles_xml_engine_free_doc_xpath(aac_bfi.doc);
  aac_bfi.doc = NULL;

  return aac_bfi.format_id;
}


void
gnac_profiles_aac_generate_pipeline(void)
{
  gchar *pipeline = gnac_profiles_default_generate_pipeline(&aac_bfi);
  pipeline = gnac_profiles_utils_add_properties(pipeline, &aac_bfi,
      "combo-bitrate", "combo-outputformat",
      "combo-profile", "checkbutton-tns", NULL);
  
  gnac_profiles_properties_update_textbuffer(pipeline);

  g_free(aac_bfi.pipeline);

  aac_bfi.pipeline = pipeline;
}


static void
gnac_profiles_aac_reset_ui(void)
{
  gnac_profiles_default_reset_ui(&aac_bfi);
  gnac_profiles_utils_reset_values(&aac_bfi,
      "combo-bitrate", "combo-outputformat",
      "combo-profile", "checkbutton-tns", NULL);
}


static void
gnac_profiles_aac_set_fields(gpointer data)
{
  if (!data) {
    gnac_profiles_aac_reset_ui();
    return;
  }

  AudioProfileAAC *profile = (AudioProfileAAC *) data;
  gnac_profiles_utils_set_values(&aac_bfi,
      "combo-bitrate", profile->bitrate,
      "combo-outputformat", profile->outputformat,
      "combo-profile", profile->profile,
      "checkbutton-tns", profile->tns,
      NULL);
}


static gchar *
gnac_profiles_aac_get_combo_format_name(void)
{
  return gnac_profiles_default_get_combo_format_name(&aac_bfi);
}


static void
gnac_profiles_aac_free_audio_profile(gpointer data)
{
  if (!data) return;

  AudioProfileAAC *profile = (AudioProfileAAC *) data;

  gnac_profiles_utils_free_audio_profile_generic(profile->generic); 

  g_free(profile->bitrate);
  g_free(profile->outputformat);
  g_free(profile->profile);
  g_free(profile->tns);
  g_free(profile);
}


static gpointer
gnac_profiles_aac_generate_audio_profile(GError **error)
{
  AudioProfileGeneric *generic = gnac_profiles_default_generate_audio_profile(
      &aac_bfi);
  AudioProfileAAC *profile = g_malloc(sizeof(AudioProfileAAC));

  profile->generic = generic;

  gnac_profiles_utils_get_values_and_set(&aac_bfi,
      "combo-bitrate", &profile->bitrate,
      "combo-outputformat", &profile->outputformat,
      "combo-profile", &profile->profile,
      "checkbutton-tns", &profile->tns,
      NULL);

  return profile;
}


static GtkWidget *
gnac_profiles_aac_get_widget(void)
{
  return gnac_profiles_default_get_properties_alignment(&aac_bfi);
}


static void
gnac_profiles_aac_save_profile(gpointer data)
{
  if (!data) return;

  AudioProfileAAC *profile = (AudioProfileAAC *) data;
  XMLDoc *doc = gnac_profiles_default_save_profile(profile->generic, &aac_bfi);
  gnac_profiles_xml_engine_add_values(doc,
      "bitrate", profile->bitrate,
      "outputformat", profile->outputformat,
      "profile", profile->profile,
      "tns", profile->tns,
      NULL);
  gnac_profiles_xml_engine_save_doc(doc, profile->generic->name);

  gnac_profiles_xml_engine_free_doc_xpath(doc);
}


static gpointer
gnac_profiles_aac_load_specific_properties(XMLDoc              *doc,
                                           AudioProfileGeneric *generic)
{
  AudioProfileAAC *profile = g_malloc(sizeof(AudioProfileAAC));

  profile->generic = generic;
  gnac_profiles_utils_load_saved_profile(doc,
      "/audio-profile/format-specific/",
      "bitrate", &profile->bitrate,
      "outputformat", &profile->outputformat,
      "profile", &profile->profile,
      "tns", &profile->tns,
      NULL);
  
  return profile;
}


static void
gnac_profiles_aac_clean_up(void)
{
  gnac_profiles_default_clean_up(&aac_bfi);
  gnac_profiles_utils_free_values(&aac_bfi,
      "combo-bitrate", "combo-outputformat",
      "combo-profile", "checkbutton-tns", NULL);
  gnac_profiles_utils_free_basic_format_info(&aac_bfi);
}


static const gchar *
gnac_profiles_aac_get_plugin_name(void)
{
  return aac_bfi.format_plugin_name;
}


static const gchar *
gnac_profiles_aac_get_description(void)
{
  return aac_bfi.description;
}


FormatModuleFuncs
gnac_profiles_aac_get_funcs(void)
{
  FormatModuleFuncs funcs = {
    gnac_profiles_aac_init,
    gnac_profiles_aac_get_description,
    gnac_profiles_aac_generate_pipeline,
    gnac_profiles_aac_generate_audio_profile,
    gnac_profiles_aac_free_audio_profile,
    gnac_profiles_aac_set_fields,
    gnac_profiles_aac_get_widget,
    gnac_profiles_aac_save_profile,
    gnac_profiles_aac_load_specific_properties,
    gnac_profiles_aac_clean_up,
    NULL,
    gnac_profiles_aac_get_combo_format_name,
    gnac_profiles_aac_get_plugin_name
  };

  return funcs;
}
