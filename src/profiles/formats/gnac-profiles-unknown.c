/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include <glib/gi18n.h>
#include <glib/gstdio.h>

#include "gnac-main.h"
#include "gnac-ui-utils.h"
#include "gnac-utils.h"
#include "../gnac-profiles-properties.h"
#include "gnac-profiles-unknown.h"


BasicFormatInfo unknown_bfi = {
  PKGDATADIR "/profiles/gnac-profiles-unknown.xml",
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  "custom-format",
  "audioconvert",
  N_("Custom format"),
  NULL,
  N_("Use this format to define your own GStreamer pipeline.")
};

const gchar *base_pipeline = 
    "audioconvert ! audio/x-raw-float, rate=44100, channels=2 ! audioconvert !";

static GtkTextView *pipeline_text_view;
static GtkWidget   *pipeline_box;

static gulong text_view_handler_id = 0;


static gboolean
gnac_profiles_unknown_text_view_handler(GtkWidget *widget,
                                        gpointer   data);


static const gchar *
gnac_profiles_unknown_init(void)
{
  unknown_bfi.builder = gnac_ui_utils_create_gtk_builder(
      unknown_bfi.gtkbuilder_xml_file);

  return unknown_bfi.format_id;
}


static void
gnac_profiles_unknown_set_pipeline_text_view(GtkTextView *text_view,
                                             GtkWidget   *box)
{
  pipeline_text_view = text_view;
  pipeline_box = box;
}


static void
gnac_profiles_unknown_generate_pipeline(void)
{

}


static void
gnac_profiles_unknown_reset_ui(void)
{
  GtkWidget *widget = gnac_profiles_utils_get_widget(&unknown_bfi,
      "entry-extension");
  gtk_entry_set_text(GTK_ENTRY(widget), "");
  if (unknown_bfi.pipeline) {
    g_free(unknown_bfi.pipeline);
    unknown_bfi.pipeline = NULL;
  }
}


static void
gnac_profiles_unknown_set_fields(gpointer data)
{
  if (!data) {
    gnac_profiles_unknown_reset_ui();
    return;
  }

  AudioProfileGeneric *profile = (AudioProfileGeneric *) data;
  GtkWidget *widget = gnac_profiles_utils_get_widget(&unknown_bfi,
      "entry-extension");
  gtk_entry_set_text(GTK_ENTRY(widget), profile->extension);
  GtkTextBuffer *text_buffer = gtk_text_view_get_buffer(
      GTK_TEXT_VIEW(pipeline_text_view));
  gtk_text_buffer_set_text(text_buffer, profile->pipeline,
      g_utf8_strlen(profile->pipeline, -1));
  unknown_bfi.pipeline = g_strdup(profile->pipeline);
}


static gchar *
gnac_profiles_unknown_get_combo_format_name(void)
{
  return g_strdup(unknown_bfi.format_name);
}


static void
gnac_profiles_unknown_free_audio_profile(gpointer data)
{
  AudioProfileGeneric *profile = (AudioProfileGeneric *) data;
  gnac_profiles_utils_free_audio_profile_generic(profile); 
}


static gpointer
gnac_profiles_unknown_generate_audio_profile(GError **error)
{
  GtkWidget *widget = gnac_profiles_utils_get_widget(&unknown_bfi,
      "entry-extension");
  const gchar *extension = gtk_entry_get_text(GTK_ENTRY(widget));

  if (gnac_utils_string_is_null_or_empty(extension)) {
    if (error && !*error) {
      *error = g_error_new(g_quark_from_static_string("gnac"), 0,
          _("The extension field must be non-empty")); 
    }
    return NULL;
  }

  AudioProfileGeneric *profile = gnac_profiles_utils_audio_profile_generic_new();
  profile->extension = g_strdup(extension);
  profile->format_id = g_strdup(unknown_bfi.format_id);
  profile->format_name = g_strdup(unknown_bfi.format_name);
  profile->channels = g_strdup("-1");
  profile->rate = g_strdup("-1");

  return profile;
}


static GtkWidget *
gnac_profiles_unknown_get_widget(void)
{
  gtk_text_view_set_editable(pipeline_text_view, TRUE);

  if (!g_signal_handler_is_connected(G_OBJECT(pipeline_text_view),
          text_view_handler_id))
  {
    gtk_widget_show(GTK_WIDGET(pipeline_box));

    text_view_handler_id = g_signal_connect(G_OBJECT(pipeline_text_view),
        "focus-out-event",
        G_CALLBACK(gnac_profiles_unknown_text_view_handler),
        NULL);
  
    GtkTextBuffer *text_buffer = gtk_text_view_get_buffer(
        GTK_TEXT_VIEW(pipeline_text_view));
    
    if (unknown_bfi.pipeline) {
      gtk_text_buffer_set_text(text_buffer, unknown_bfi.pipeline,
          g_utf8_strlen(unknown_bfi.pipeline, -1));
    } else {
      gtk_text_buffer_set_text(text_buffer, base_pipeline,
          g_utf8_strlen(base_pipeline, -1));
    }
  }

  return gnac_profiles_default_get_properties_alignment(&unknown_bfi);
}


static void
gnac_profiles_unknown_save_profile(gpointer data)
{
  if (!data) return;

  AudioProfileGeneric *profile = (AudioProfileGeneric *) data;
  XMLDoc *doc = gnac_profiles_default_save_profile(profile, &unknown_bfi);
  gnac_profiles_xml_engine_save_doc(doc, profile->generic->name);
  gnac_profiles_xml_engine_free_doc_xpath(doc);
}


static gpointer
gnac_profiles_unknown_load_specific_properties(XMLDoc              *doc,
                                               AudioProfileGeneric *generic)
{
  return generic;
}


static void
gnac_profiles_unknown_clean_up(void)
{
  GtkWidget *widget = gnac_profiles_default_get_properties_alignment(
      &unknown_bfi);
  g_free(unknown_bfi.pipeline);
  g_object_unref(unknown_bfi.builder);
  gtk_widget_destroy(widget);
}


static const gchar *
gnac_profiles_unknown_get_plugin_name(void)
{
  return unknown_bfi.format_plugin_name;
}


static const gchar *
gnac_profiles_unknown_get_description(void)
{
  return unknown_bfi.description;
}


static gboolean
gnac_profiles_unknown_text_view_handler(GtkWidget *widget,
                                        gpointer   data)
{
  g_free(unknown_bfi.pipeline);

  unknown_bfi.pipeline = gnac_profiles_utils_text_view_get_text(
      GTK_TEXT_VIEW(pipeline_text_view));

  return FALSE;
}


void
gnac_profiles_unknown_on_hide(GtkWidget *widget,
                              gpointer   data)
{
  if (g_signal_handler_is_connected(G_OBJECT(pipeline_text_view),
          text_view_handler_id))
  {
    g_signal_handler_disconnect(G_OBJECT(pipeline_text_view),
        text_view_handler_id);
  }
}


FormatModuleFuncs
gnac_profiles_unknown_get_funcs(void)
{
  FormatModuleFuncs funcs = {
    gnac_profiles_unknown_init,
    gnac_profiles_unknown_get_description,
    gnac_profiles_unknown_generate_pipeline,
    gnac_profiles_unknown_generate_audio_profile,
    gnac_profiles_unknown_free_audio_profile,
    gnac_profiles_unknown_set_fields,
    gnac_profiles_unknown_get_widget,
    gnac_profiles_unknown_save_profile,
    gnac_profiles_unknown_load_specific_properties,
    gnac_profiles_unknown_clean_up,
    gnac_profiles_unknown_set_pipeline_text_view,
    gnac_profiles_unknown_get_combo_format_name,
    gnac_profiles_unknown_get_plugin_name
  };

  return funcs;
}
