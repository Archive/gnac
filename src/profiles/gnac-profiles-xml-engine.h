/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_PROFILES_XML_ENGINE_H
#define GNAC_PROFILES_XML_ENGINE_H

#include <glib.h>

#define GNAC_SAVED_PROFILES_URL(name) \
    saved_profiles_dir , G_DIR_SEPARATOR_S, name
#define GNAC_SAVED_PROFILES_URL_WITH_EXT(name) \
    GNAC_SAVED_PROFILES_URL(name) , ".xml"

G_BEGIN_DECLS

gchar *saved_profiles_dir;

typedef struct
{
  gpointer doc;
  gpointer xpath_context;
}
XMLDoc;

typedef struct
{
  gchar *value;
  gchar *name;
}
Value;

typedef struct
{
  GList *values;
  GList *names;
  gchar *default_value;
  gchar *variable_name;
}
ComboValues;

typedef struct
{
  gdouble  min;
  gdouble  max;
  gdouble  step;
  gdouble  default_value;
  gchar   *variable_name;
}
SliderValues;

typedef struct
{
  gboolean  default_value;
  gchar    *variable_name;
}
CheckValues;

XMLDoc *
gnac_profiles_xml_engine_load_doc_xpath(const gchar *filename);

gchar *
gnac_profiles_xml_engine_get_format_id(XMLDoc *doc);

gchar *
gnac_profiles_xml_engine_get_variable_type(XMLDoc      *doc,
                                           const gchar *expr);

gchar *
gnac_profiles_xml_engine_get_text_node(XMLDoc      *doc,
                                       const gchar *expr);

gchar *
gnac_profiles_xml_engine_query_name(XMLDoc      *doc,
                                    const gchar *format);

gchar *
gnac_profiles_xml_engine_query_description(XMLDoc      *doc,
                                           const gchar *format);

gchar *
gnac_profiles_xml_engine_query_extension(XMLDoc      *doc,
                                         const gchar *format);

GList *
gnac_profiles_xml_engine_get_list_values(XMLDoc      *doc,
                                         const gchar *expr);

ComboValues *
gnac_profiles_xml_engine_get_combo_values(XMLDoc      *doc,
                                          const gchar *expr) G_GNUC_MALLOC;
SliderValues *
gnac_profiles_xml_engine_get_slider_values(XMLDoc      *doc,
                                          const gchar  *expr) G_GNUC_MALLOC;
CheckValues *
gnac_profiles_xml_engine_get_check_values(XMLDoc      *doc,
                                          const gchar *expr) G_GNUC_MALLOC;

void
gnac_profiles_xml_engine_modify_values(XMLDoc      *doc,
                                       const gchar *expr,
                                       ...);

void
gnac_profiles_xml_engine_add_values(XMLDoc *doc, ...);

void
gnac_profiles_xml_engine_save_doc(XMLDoc      *doc,
                                  const gchar *filename);

void
gnac_profiles_xml_engine_free_doc_xpath(XMLDoc *doc);

void
gnac_profiles_xml_engine_free_combo_values(ComboValues *values);

void
gnac_profiles_xml_engine_free_slider_values(SliderValues *values);

void
gnac_profiles_xml_engine_free_check_values(CheckValues *values);

gchar *
gnac_profiles_xml_engine_format_text_to_xml(const gchar *text);

G_END_DECLS

#endif /* GNAC_PROFILES_XML_ENGINE_H */
