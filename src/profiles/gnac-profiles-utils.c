/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "gnac-profiles-utils.h"
#include "gnac-ui-utils.h"
#include "gnac-utils.h"
#include "libgnac-debug.h"

static gchar *raw = NULL;
static gchar *audioconvert = NULL;


gchar *
gnac_profiles_utils_get_combo_format_name(const gchar *name,
                                          const gchar *extension)
{
  return g_strconcat(name, " (.", extension, ")", NULL);
}


static void
gnac_profiles_utils_add_values_combo(GtkWidget   *combo,
                                     ComboValues *combo_values)
{
  gint index = 0;
  GList *names = combo_values->names;
  GList *values = combo_values->values;

  while (names) {
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo), names->data);

    if (gnac_utils_str_equal(combo_values->default_value, values->data)) {
      gtk_combo_box_set_active(GTK_COMBO_BOX(combo), index);
    }

    names = g_list_next(names);
    values = g_list_next(values);
    index++;
  }
}


static void
gnac_profiles_utils_register_combo(GtkWidget   *widget,
                                   XMLDoc      *doc,
                                   const gchar *xpath_query)
{
  ComboValues *values = gnac_profiles_xml_engine_get_combo_values(doc,
      xpath_query);
  if (!values) {
    libgnac_warning("Impossible to register combo with xpath query %s",
        xpath_query);
    return;
  }

  gnac_profiles_utils_add_values_combo(widget, values);
  g_object_set_data(G_OBJECT(widget), "combo-values", values);
}


static void
gnac_profiles_utils_add_values_slider(GtkWidget    *slider,
                                      SliderValues *values)
{
  gtk_range_set_range(GTK_RANGE(slider), values->min, values->max);
  gtk_range_set_value(GTK_RANGE(slider), values->default_value);
  gtk_range_set_increments(GTK_RANGE(slider), values->step, values->step);
}


static void
gnac_profiles_utils_register_slider(GtkWidget   *widget,
                                    XMLDoc      *doc,
                                    const gchar *xpath_query)
{
  SliderValues *values = gnac_profiles_xml_engine_get_slider_values(doc,
      xpath_query);
  if (!values) {
    libgnac_warning("Impossible to register slider with xpath query %s",
        xpath_query);
    return;
  }

  gnac_profiles_utils_add_values_slider(widget, values);
  g_object_set_data(G_OBJECT(widget), "slider-values", values);
}


static void
gnac_profiles_utils_register_check(GtkWidget   *widget,
                                   XMLDoc      *doc,
                                   const gchar *xpath_query)
{
  CheckValues *values = gnac_profiles_xml_engine_get_check_values(doc,
      xpath_query);
  if (!values) {
    libgnac_warning("Impossible to register check with xpath query %s",
        xpath_query);
    return;
  }

  g_object_set_data(G_OBJECT(widget), "check-values", values);
}


GtkWidget *
gnac_profiles_utils_init_base_widget(GtkBuilder  *builder,
                                     XMLDoc      *doc,
                                     const gchar *widget_name,
                                     const gchar *xpath_query)
{
  GtkWidget *widget = gnac_ui_utils_get_widget(builder, widget_name);
  if (!widget) return NULL;

  gchar *widget_type = gnac_profiles_xml_engine_get_variable_type(doc,
      xpath_query);

  if (gnac_utils_str_equal(widget_type, "combo"))  {
    gnac_profiles_utils_register_combo(widget, doc, xpath_query);
  } else if (gnac_utils_str_equal(widget_type, "check")) {
    gnac_profiles_utils_register_check(widget, doc, xpath_query);
  } else if (gnac_utils_str_equal(widget_type, "slider")) {
    gnac_profiles_utils_register_slider(widget, doc, xpath_query);
  }

  g_free(widget_type);

  return widget;
}


GtkWidget *
gnac_profiles_utils_init_widget(BasicFormatInfo *bfi,
                                const gchar     *widget_name,
                                const gchar     *xpath_query)
{
  return gnac_profiles_utils_init_base_widget(bfi->builder, bfi->doc,
      widget_name, xpath_query);
}


static void
gnac_profiles_utils_set_value_combo(GtkWidget   *combo,
                                    const gchar *value)
{
  ComboValues *combo_values = g_object_get_data(G_OBJECT(combo), "combo-values");
  GList *values = combo_values->values;

  if (!value) value = combo_values->default_value;

  gint index = 0;
  while (values) {
    if (gnac_utils_str_equal(value,  values->data)) {
      gtk_combo_box_set_active(GTK_COMBO_BOX(combo), index);
    }

    values = g_list_next(values);
    index++;
  }
}


static void
gnac_profiles_utils_set_value_check(GtkWidget   *widget,
                                    const gchar *value)
{
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(widget),
      gnac_utils_str_equal(value, "true"));
}


static void
gnac_profiles_utils_set_value_slider(GtkWidget     *widget,
                                     const gdouble  value)
{
  gtk_range_set_value(GTK_RANGE(widget), value);
}


void
gnac_profiles_utils_set_values(BasicFormatInfo *bfi, ...)
{
  va_list ap;
  va_start(ap, bfi);

  const gchar *name = va_arg(ap, const gchar *);

  while (name) {
    GtkWidget *widget = gnac_profiles_utils_get_widget(bfi, name);

    if (GTK_IS_COMBO_BOX(widget)) {
      const gchar *value = va_arg(ap, const gchar *);
      gnac_profiles_utils_set_value_combo(widget, value);
    } else if (GTK_IS_CHECK_BUTTON(widget)) {
      const gchar *value = va_arg(ap, const gchar *);
      gnac_profiles_utils_set_value_check(widget, value);
    } else if (GTK_IS_RANGE(widget)) {
      gdouble value = va_arg(ap, gdouble);
      gnac_profiles_utils_set_value_slider(widget, value);
    } else {
      libgnac_debug("Unhandled widget type: %s",
          g_type_name(G_OBJECT_TYPE(widget)));
    }

    name = va_arg(ap, const gchar *);
  }

  va_end(ap);
}


static void
gnac_profiles_utils_reset_value_combo(GtkWidget *widget)
{
  ComboValues *values = g_object_get_data(G_OBJECT(widget), "combo-values");
  gnac_profiles_utils_set_value_combo(widget, values->default_value);
}


static void
gnac_profiles_utils_reset_value_check(GtkWidget *widget)
{
  CheckValues *values = g_object_get_data(G_OBJECT(widget), "check-values");
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(widget), values->default_value);
}


static void
gnac_profiles_utils_reset_value_slider(GtkWidget *widget)
{
  SliderValues *values = g_object_get_data(G_OBJECT(widget), "slider-values");
  gtk_range_set_value(GTK_RANGE(widget), values->default_value);
}


void
gnac_profiles_utils_reset_values(BasicFormatInfo *bfi, ...)
{
  va_list ap;
  va_start(ap, bfi);

  const gchar *name = va_arg(ap, const gchar *);

  while (name) {
    GtkWidget *widget = gnac_profiles_utils_get_widget(bfi, name);

    if (GTK_IS_COMBO_BOX(widget)) {
      gnac_profiles_utils_reset_value_combo(widget);
    } else if (GTK_IS_CHECK_BUTTON(widget)) {
      gnac_profiles_utils_reset_value_check(widget);
    } else if (GTK_IS_RANGE(widget)) {
      gnac_profiles_utils_reset_value_slider(widget);
    } else {
      libgnac_debug("Unhandled widget type: %s",
          g_type_name(G_OBJECT_TYPE(widget)));
    }

    name = va_arg(ap, const gchar *);
  }

  va_end(ap);
}


void
gnac_profiles_utils_set_values_checked(BasicFormatInfo *bfi, ...)
{
  va_list ap;
  va_start(ap, bfi);

  const gchar *widget_name = va_arg(ap, const gchar *);

  while (widget_name) {
    gboolean active = FALSE;
    const gchar *checkbox_name = va_arg(ap, const gchar *);
    GtkWidget *widget = gnac_profiles_utils_get_widget(bfi, widget_name);

    if (GTK_IS_COMBO_BOX(widget)) {
      const gchar *value = va_arg(ap, const gchar *);
      if (value) {
        active = TRUE;
        gnac_profiles_utils_set_value_combo(widget, value);
      }
    } else if (GTK_IS_RANGE(widget)) {
      gdouble value = va_arg(ap, gdouble);
      if (value != -1) {
        active = TRUE;
        gtk_range_set_value(GTK_RANGE(widget), value);
      }
    } else {
      libgnac_debug("Unhandled widget type: %s",
          g_type_name(G_OBJECT_TYPE(widget)));
    }

    widget = gnac_profiles_utils_get_widget(bfi, checkbox_name);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(widget), active);

    widget_name = va_arg(ap, const gchar *);
  }

  va_end(ap);
}


gchar *
gnac_profiles_utils_get_value_combo(GtkWidget *combo)
{
  gint index = gtk_combo_box_get_active(GTK_COMBO_BOX(combo));
  ComboValues *values = g_object_get_data(G_OBJECT(combo), "combo-values");
  if (!values) return NULL;

  return g_strdup(g_list_nth_data(values->values, index));
}


void
gnac_profiles_utils_get_values_and_set(BasicFormatInfo *bfi, ...)
{
  va_list ap;
  va_start(ap, bfi);

  const gchar *name = va_arg(ap, const gchar *);

  while (name) {
    GtkWidget *widget = gnac_profiles_utils_get_widget(bfi, name);

    if (GTK_IS_COMBO_BOX(widget)) {
      const gchar **value = va_arg(ap, const gchar **);
      *value = gnac_profiles_utils_get_value_combo(widget);
    } else if (GTK_IS_CHECK_BUTTON(widget)) {
      const gchar **value = va_arg(ap, const gchar **);
      *value = g_strdup(gtk_toggle_button_get_active(
          GTK_TOGGLE_BUTTON(widget)) ? "true" : "false");
    } else if (GTK_IS_RANGE(widget)) {
      gdouble *value = va_arg(ap, gdouble *);
      *value = gtk_range_get_value(GTK_RANGE(widget));
    } else {
      libgnac_debug("Unhandled widget type: %s",
          g_type_name(G_OBJECT_TYPE(widget)));
    }
    
    name = va_arg(ap, const gchar *);
  }

  va_end(ap);
}


void
gnac_profiles_utils_get_values_checked_combo_and_set(BasicFormatInfo *bfi, ...)
{
  va_list ap;
  va_start(ap, bfi);

  const gchar *name = va_arg(ap, const gchar *);

  while (name) {
    const gchar *checkbox_name = va_arg(ap, const gchar *);
    const gchar **value = va_arg(ap, const gchar **);
    GtkWidget *widget = gnac_profiles_utils_get_widget(bfi, checkbox_name);

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
      widget = gnac_profiles_utils_get_widget(bfi, name);
      *value = gnac_profiles_utils_get_value_combo(widget);
    }

    name = va_arg(ap, const gchar *);
  }

  va_end(ap);
}


void
gnac_profiles_utils_get_values_checked_slider_and_set(BasicFormatInfo *bfi, ...)
{
  va_list ap;
  va_start(ap, bfi);

  const gchar *name = va_arg(ap, const gchar *);

  while (name) {
    const gchar *checkbox_name = va_arg(ap, const gchar *);
    gdouble *value = va_arg(ap, gdouble *);
    GtkWidget *widget = gnac_profiles_utils_get_widget(bfi, checkbox_name);

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
      widget = gnac_profiles_utils_get_widget(bfi, name);
      *value = gtk_range_get_value(GTK_RANGE(widget));
    }

    name = va_arg(ap, const gchar *);
  }

  va_end(ap);
}


void
gnac_profiles_utils_add_description_tooltip(BasicFormatInfo *bfi,
                                            const gchar     *xpath_query,
                                            ...)
{
  gchar *description = gnac_profiles_xml_engine_get_text_node(bfi->doc,
      xpath_query);
  if (!description) {
    libgnac_warning("Impossible to find description in %s", xpath_query);
    return;
  }

  va_list ap;
  va_start(ap, xpath_query);

  GtkWidget *widget = va_arg(ap, GtkWidget *);

  while (widget) {
    gtk_widget_set_tooltip_markup(widget, description);
    widget = va_arg(ap, GtkWidget *);
  }

  va_end(ap);

  g_free(description);
}


void
gnac_profiles_utils_init_raw_audioconvert(XMLDoc *doc)
{
  if (G_UNLIKELY(!raw)) {
    raw = gnac_profiles_xml_engine_get_text_node(doc,
        "//process[@id='gstreamer-audio']");
  }

  if (G_UNLIKELY(!audioconvert)) {
    audioconvert = gnac_profiles_xml_engine_get_text_node(doc,
        "//process[@id='audioconvert']");
  }
}


gchar *
gnac_profiles_utils_get_basepipeline(const gchar *channels,
                                     const gchar *rate)
{
  return g_strconcat(audioconvert, " ! ", raw, ", rate=", rate,
      ", channels=", channels, " ! ", audioconvert, NULL);
}


gchar *
gnac_profiles_utils_add_pipe(gchar       *pipeline,
                             const gchar *new_pipe)
{
  gchar *temp = g_strconcat(pipeline, " ! ", new_pipe, NULL);
  g_free(pipeline);
  return temp;
}


gchar *
gnac_profiles_utils_add_pipes(gchar *pipeline,
                              GList *values)
{
  GList *list_temp = values;

  while (list_temp) {
    Value *v = list_temp->data;
    gchar *temp;

    if (gnac_utils_string_is_null_or_empty(pipeline)) {
      temp = g_strdup(v->value);
    } else {
      temp = g_strconcat(pipeline, " ! ", v->value, NULL);
    }

    g_free(pipeline);
    g_free(v->name);
    g_free(v->value);
    g_free(v);

    pipeline = temp;
    list_temp = g_list_next(list_temp);
  }

  return pipeline;
}


static gchar *
gnac_profiles_utils_add_property_combo(gchar     *pipeline,
                                       GtkWidget *combo)
{
  gint index = gtk_combo_box_get_active(GTK_COMBO_BOX(combo));
  ComboValues *values = g_object_get_data(G_OBJECT(combo), "combo-values");
  if (!values) return pipeline;

  gchar *value = g_list_nth_data(values->values, index);
  gchar *temp = g_strconcat(pipeline, " ",
      values->variable_name, "=", value, NULL);

  g_free(pipeline);

  return temp;
}


static gchar *
gnac_profiles_utils_add_property_check(gchar     *pipeline,
                                       GtkWidget *widget)
{
  gboolean checked = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget));
  CheckValues *values = g_object_get_data(G_OBJECT(widget), "check-values");
  if (!values) return pipeline;

  gchar *temp = g_strconcat(pipeline, " ",
      values->variable_name, "=", checked ? "true" : "false", NULL);

  g_free(pipeline);

  return temp;
}


gchar *
gnac_profiles_utils_add_properties(gchar           *pipeline,
                                   BasicFormatInfo *bfi,
                                   ...)
{
  va_list ap;
  va_start(ap, bfi);

  const gchar *name = va_arg(ap, const gchar *);

  while (name) {
    GtkWidget *widget = gnac_profiles_utils_get_widget(bfi, name);

    if (GTK_IS_COMBO_BOX(widget)) {
      pipeline = gnac_profiles_utils_add_property_combo(pipeline, widget);
    } else if (GTK_IS_CHECK_BUTTON(widget)) {
      pipeline = gnac_profiles_utils_add_property_check(pipeline, widget);
    } else {
      libgnac_debug("Unhandled widget type: %s",
          g_type_name(G_OBJECT_TYPE(widget)));
    }

    name = va_arg(ap, const gchar *);
  }

  va_end(ap);

  return pipeline;
}


gchar *
gnac_profiles_utils_add_properties_checked_combo(gchar           *pipeline,
                                                 BasicFormatInfo *bfi,
                                                 ...)
{
  va_list ap;
  va_start(ap, bfi);

  const gchar *name = va_arg(ap, const gchar *);

  while (name) {
    const gchar *checkbox_name = va_arg(ap, const gchar *);
    GtkWidget *widget = gnac_profiles_utils_get_widget(bfi, checkbox_name);

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
      widget = gnac_profiles_utils_get_widget(bfi, name);
      pipeline = gnac_profiles_utils_add_property_combo(pipeline, widget);
    }

    name = va_arg(ap, const gchar *);
  }

  va_end(ap);

  return pipeline;
}


gchar *
gnac_profiles_utils_add_property_slider(gchar       *pipeline,
                                        const gchar *format,
                                        gdouble      factor,
                                        GtkWidget   *slider)
{
  gdouble value = gtk_range_get_value(GTK_RANGE(slider));
  SliderValues *values = g_object_get_data(G_OBJECT(slider), "slider-values");
  if (!values) return pipeline;

  gchar *value_str = gnac_profiles_utils_gdouble_to_gchararray_format(
      value * factor, format);
  gchar *temp = g_strconcat(pipeline, " ",
      values->variable_name, "=", value_str, NULL);

  g_free(pipeline);
  g_free(value_str);

  return temp;
}


gchar *
gnac_profiles_utils_add_properties_slider(gchar           *pipeline,
                                          BasicFormatInfo *bfi,
                                          const gchar     *format,
                                          ...)
{
  va_list ap;
  va_start(ap, format);

  const gchar *name = va_arg(ap, const gchar *);

  while (name) {
    GtkWidget *widget = gnac_profiles_utils_get_widget(bfi, name);
    pipeline = gnac_profiles_utils_add_property_slider(pipeline, format, 1,
        widget);
    name = va_arg(ap, const gchar *);
  }

  va_end(ap);

  return pipeline;
}


gchar *
gnac_profiles_utils_add_properties_checked_slider(gchar           *pipeline,
                                                  BasicFormatInfo *bfi,
                                                  ...)
{
  va_list ap;
  va_start(ap, bfi);

  const gchar *name = va_arg(ap, const gchar *);

  while (name) {
    const gchar *checkbox_name = va_arg(ap, const gchar *);
    GtkWidget *widget = gnac_profiles_utils_get_widget(bfi, checkbox_name);

    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget))) {
      widget = gnac_profiles_utils_get_widget(bfi, name);
      pipeline = gnac_profiles_utils_add_property_slider(pipeline, "%.0f", 1,
          widget);
    }

    name = va_arg(ap, const gchar *);
  }

  va_end(ap);

  return pipeline;
}


void
gnac_profiles_utils_set_active_toggle_button(BasicFormatInfo *bfi,
                                             gboolean         active,
                                             ...)
{
  va_list ap;
  va_start(ap, active);

  const gchar *name = va_arg(ap, const gchar *);

  while (name) {
    GtkWidget *widget = gnac_profiles_utils_get_widget(bfi, name);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(widget), active);
    name = va_arg(ap, const gchar *);
  }

  va_end(ap);
}


void
gnac_profiles_utils_on_toggle_optionnal_property(GtkToggleButton *togglebutton,
                                                 gpointer         user_data)
{
  GtkWidget *widget = GTK_WIDGET(user_data);
  gtk_widget_set_sensitive(widget, gtk_toggle_button_get_active(togglebutton));
}


void
gnac_profiles_utils_load_saved_profile(XMLDoc      *doc,
                                       const gchar *base_query,
                                       ...)
{
  va_list ap;
  va_start(ap, base_query);

  const gchar *spec_query = va_arg(ap, const gchar *);

  while (spec_query) {
    gchar **value = va_arg(ap, gchar **);
    gchar *query = g_strconcat(base_query, spec_query, NULL);
    *value = gnac_profiles_xml_engine_get_text_node(doc, query);
    g_free(query);
    spec_query = va_arg(ap, const gchar *);
  }

  va_end(ap);
}


AudioProfileGeneric *
gnac_profiles_utils_audio_profile_generic_new(void)
{
  AudioProfileGeneric *profile = g_malloc(sizeof(AudioProfileGeneric));

  profile->generic = profile;
  profile->name = NULL;
  profile->description = NULL;
  profile->format_id = NULL;
  profile->format_name = NULL;
  profile->extension = NULL;
  profile->pipeline = NULL;
  profile->rate = NULL;
  profile->channels = NULL;
  
  return profile;
}


void
gnac_profiles_utils_free_audio_profile_generic(AudioProfileGeneric *profile)
{
  if (!profile) return;

  g_free(profile->name);
  g_free(profile->description);
  g_free(profile->format_id);
  g_free(profile->format_name);
  g_free(profile->extension);
  g_free(profile->pipeline);
  g_free(profile->rate);
  g_free(profile->channels);
  g_free(profile);
}


void
gnac_profiles_utils_free_basic_format_info(BasicFormatInfo *bfi)
{
  if (!bfi) return;

  g_object_unref(bfi->builder);
  gnac_profiles_xml_engine_free_doc_xpath(bfi->doc);
  g_free(bfi->pipeline);
  g_free(bfi->pipeline_encoder);
  g_free(bfi->pipeline_multiplexers);
  g_free(bfi->format_id);
  g_free(bfi->format_plugin_name);
  g_free(bfi->format_name);
  g_free(bfi->file_extension);
  g_free(bfi->description);
}


void
gnac_profiles_utils_free_values(BasicFormatInfo *bfi, ...)
{
  va_list ap;
  va_start(ap, bfi);

  const gchar *name = va_arg(ap, const gchar *);

  while (name) {
    GtkWidget *widget = gnac_profiles_utils_get_widget(bfi, name);

    if (GTK_IS_COMBO_BOX(widget)) {
      ComboValues *values = g_object_get_data(G_OBJECT(widget), "combo-values");
      gnac_profiles_xml_engine_free_combo_values(values);
    } else if (GTK_IS_CHECK_BUTTON(widget)) {
      CheckValues *values = g_object_get_data(G_OBJECT(widget), "check-values");
      gnac_profiles_xml_engine_free_check_values(values);
    } else if (GTK_IS_RANGE(widget)) {
      SliderValues *values = g_object_get_data(G_OBJECT(widget), "slider-values");
      gnac_profiles_xml_engine_free_slider_values(values);
    } else {
      libgnac_debug("Unhandled widget type: %s",
          g_type_name(G_OBJECT_TYPE(widget)));
    }

    name = va_arg(ap, const gchar *);
  }

  va_end(ap);
}


gchar *
gnac_profiles_utils_gdouble_to_gchararray(gdouble value)
{
  return gnac_profiles_utils_gdouble_to_gchararray_format(value, "%.0f");
}


gchar *
gnac_profiles_utils_gdouble_to_gchararray_format(gdouble      value,
                                                 const gchar *format)
{
  gchar buffer[64];
  g_ascii_formatd(buffer, 64, format, value);
  return g_strdup(buffer);
}


gdouble
gnac_profiles_utils_gchararray_to_gdouble(const gchar *value)
{
  return g_ascii_strtod(value, NULL);
}


GtkWidget *
gnac_profiles_utils_get_widget(BasicFormatInfo *bfi,
                               const gchar     *widget_name)
{
  return gnac_ui_utils_get_widget(bfi->builder, widget_name);
}


gchar *
gnac_profiles_utils_text_view_get_text(GtkTextView *text_view)
{

  GtkTextBuffer *buffer = gtk_text_view_get_buffer(text_view);

  GtkTextIter start;
  gtk_text_buffer_get_start_iter(buffer, &start);
  GtkTextIter end;
  gtk_text_buffer_get_end_iter(buffer, &end);

  return gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
}
