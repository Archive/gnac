/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_PROFILES_MANAGER_H
#define GNAC_PROFILES_MANAGER_H

#include "gnac-profiles-properties.h"
#include "gnac-profiles-utils.h"

G_BEGIN_DECLS

void
gnac_profiles_mgr_init(void);

void
gnac_profiles_mgr_list_profiles(void);

GList *
gnac_profiles_mgr_get_profiles_list(void);

void
gnac_profiles_mgr_destroy(void);

void
gnac_profiles_mgr_show(void);

void
gnac_profiles_mgr_on_drag_data_received(GtkWidget        *widget,
                                        GdkDragContext   *context,
                                        gint              x,
                                        gint              y,
                                        GtkSelectionData *selection_data,
                                        guint             info,
                                        guint             time,
                                        gpointer          data);

void
gnac_profiles_mgr_on_drag_data_get(GtkWidget        *widget,
                                   GdkDragContext   *drag_context,
                                   GtkSelectionData *data,
                                   guint             info,
                                   guint             time,
                                   gpointer          user_data);

void
gnac_profiles_mgr_on_add(GtkWidget *widget,
                         gpointer   data);

void
gnac_profiles_mgr_on_copy(GtkWidget *widget,
                          gpointer   data);

void
gnac_profiles_mgr_on_edit(GtkWidget *widget,
                          gpointer   data);

void
gnac_profiles_mgr_on_remove(GtkWidget *widget,
                            gpointer   data);

void
gnac_profiles_mgr_on_close(GtkWidget *widget,
                           gpointer   data);

gboolean
gnac_profiles_mgr_on_delete_event(GtkWidget *widget,
                                  GdkEvent  *event,
                                  gpointer   user_data);

gboolean
gnac_profiles_mgr_list_on_key_pressed(GtkWidget   *widget,
                                      GdkEventKey *event,
                                      gpointer     data);

gboolean
gnac_profiles_mgr_on_key_pressed(GtkWidget   *widget,
                                 GdkEventKey *event,
                                 gpointer     data);

G_END_DECLS

#endif /* GNAC_PROFILES_MANAGER_H */
