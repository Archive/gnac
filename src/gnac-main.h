/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_MAIN_H
#define GNAC_MAIN_H

#include <gio/gio.h>
#include <gtk/gtk.h>

#include "libgnac-converter.h"

G_BEGIN_DECLS

typedef enum {
  GNAC_AUDIO_EMPTY_STATE,
  GNAC_AUDIO_FILE_ACTION_STATE,
  GNAC_AUDIO_READY_STATE,
  GNAC_AUDIO_CLEAR_STATE,
  GNAC_AUDIO_CONVERT_STATE,
  GNAC_AUDIO_PAUSED_STATE,
  GNAC_PLUGIN_INSTALL_STATE
} GnacState;

extern GnacState state;
extern GnacState prev_state;
extern guint nb_files_added;
extern guint nb_files_total;
extern LibgnacConverter *converter;
extern LibgnacMetadata *metadata;

void
gnac_add_file(GFile *file);

void
gnac_add_files(GSList *files);

void
gnac_on_ui_convert_cb(GtkWidget *widget, 
                      gpointer   data);

void
gnac_on_ui_pause_cb(GtkWidget *widget, 
                    gpointer   data);

G_GNUC_NORETURN void
gnac_exit(gint status);

gboolean
gnac_on_ui_destroy_cb(GtkWidget *widget, 
                      gpointer   data);

G_END_DECLS

#endif /* GNAC_MAIN_H */
