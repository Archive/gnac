/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_BARS_H
#define GNAC_BARS_H

#include <glib.h>

G_BEGIN_DECLS

void 
gnac_bars_on_row_inserted(void);

void 
gnac_bars_on_row_deleted(void);

void 
gnac_bars_on_add_files(void);

void 
gnac_bars_on_add_files_finished(void);

void 
gnac_bars_on_convert(void);

void
gnac_bars_on_convert_pause(void);

void
gnac_bars_on_convert_resume(void);

void
gnac_bars_on_convert_stop(void);

void
gnac_bars_on_plugin_install(void);

void
gnac_bars_on_plugin_installed(void);

void
gnac_bars_activate_add(gboolean activate);

void
gnac_bars_activate_clear(gboolean activate);

void
gnac_bars_activate_convert(gboolean activate);

void
gnac_bars_activate_file_list(gboolean activate);

void
gnac_bars_activate_pause(gboolean activate);

void
gnac_bars_activate_preferences(gboolean activate);

void
gnac_bars_activate_properties(gboolean activate);

void
gnac_bars_activate_remove(gboolean activate);

void
gnac_bars_activate_quit(gboolean activate);

G_END_DECLS

#endif /* GNAC_BARS_H */
