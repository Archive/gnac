/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "gnac-bars.h"
#include "gnac-main.h"
#include "gnac-stock-items.h"
#include "gnac-ui.h"


static void
gnac_bars_update_convert_label(const gchar *label)
{
  g_return_if_fail(label);

  GtkAction *action = gnac_ui_get_action("convert_item");
  g_object_set(G_OBJECT(action), "stock-id", label, NULL);

  GtkWidget *widget = gnac_ui_get_widget("convert_button");
  gtk_button_set_label(GTK_BUTTON(widget), label);
}


static void
gnac_bars_update_pause_label(const gchar *label)
{
  g_return_if_fail(label);

  GtkAction *action = gnac_ui_get_action("pause_item");
  g_object_set(G_OBJECT(action), "stock-id", label, NULL);

  action = gnac_ui_get_action("tray_pause_item");
  g_object_set(G_OBJECT(action), "stock-id", label, NULL);

  GtkWidget *widget = gnac_ui_get_widget("pause_button");
  gtk_button_set_label(GTK_BUTTON(widget), label);
}


void
gnac_bars_on_row_inserted(void)
{
  gboolean activate = (nb_files_total != 0);
  gnac_bars_activate_convert(activate);
  gnac_bars_activate_clear(activate);
}


void
gnac_bars_on_row_deleted(void)
{
  gnac_bars_activate_convert(FALSE);
  gnac_bars_activate_remove(FALSE);
  gnac_bars_activate_clear(FALSE);
}


void 
gnac_bars_on_add_files(void)
{
  gnac_bars_update_convert_label(GTK_STOCK_CANCEL);
  gnac_ui_activate_profiles(FALSE);
  gnac_bars_activate_add(FALSE);
  gnac_bars_activate_convert(TRUE);
  gnac_bars_activate_clear(FALSE);
  gnac_bars_activate_remove(FALSE);
}


void 
gnac_bars_on_add_files_finished(void)
{
  gnac_bars_update_convert_label(GTK_STOCK_CONVERT);
  gnac_ui_activate_profiles(TRUE);
  gnac_bars_activate_add(TRUE);
  gnac_bars_activate_clear(TRUE);
  gnac_bars_activate_remove(TRUE);
}


void
gnac_bars_on_convert(void)
{
  gnac_bars_activate_file_list(FALSE);
  gnac_bars_update_convert_label(GTK_STOCK_STOP);
  gnac_ui_activate_profiles(FALSE);
  gnac_bars_activate_add(FALSE);
  gnac_bars_activate_clear(FALSE);
  gnac_bars_activate_remove(FALSE);
  gnac_bars_activate_pause(TRUE);
  gnac_bars_activate_properties(FALSE);
  gnac_bars_activate_preferences(FALSE);
}


void
gnac_bars_on_convert_pause(void)
{
  gnac_bars_update_pause_label(GNAC_STOCK_RESUME);
  /* update the status bar */
  gnac_ui_append_status(_("paused"));
}


void
gnac_bars_on_convert_resume(void)
{
  gnac_bars_update_pause_label(GTK_STOCK_MEDIA_PAUSE);
}


void
gnac_bars_on_convert_stop(void)
{
  gnac_bars_activate_file_list(TRUE);
  gnac_bars_update_convert_label(GTK_STOCK_CONVERT);
  gnac_ui_activate_profiles(TRUE);
  gnac_bars_activate_add(TRUE);
  gnac_bars_activate_clear((nb_files_total != 0));
  gnac_bars_activate_pause(FALSE);
  gnac_bars_activate_preferences(TRUE);
}


void
gnac_bars_on_plugin_install(void)
{
  gnac_bars_activate_add(FALSE);
  gnac_bars_activate_clear(FALSE);
  gnac_bars_activate_convert(FALSE);
  gnac_bars_activate_pause(FALSE);
  gnac_bars_activate_preferences(FALSE);
  gnac_bars_activate_remove(FALSE);
  gnac_bars_activate_quit(FALSE);
}


void
gnac_bars_on_plugin_installed(void)
{
  gnac_bars_activate_add(TRUE);
  gnac_bars_activate_clear(TRUE);
  gnac_bars_activate_convert(TRUE);
  gnac_bars_activate_pause(TRUE);
  gnac_bars_activate_preferences(TRUE);
  gnac_bars_activate_remove(TRUE);
  gnac_bars_activate_quit(TRUE);
}


void
gnac_bars_activate_add(gboolean activate)
{
  gnac_ui_set_action_sensitive("add_item", activate);
}


void
gnac_bars_activate_clear(gboolean activate)
{
  gnac_ui_set_action_sensitive("clear_item", activate);
}


void
gnac_bars_activate_convert(gboolean activate)
{
  gnac_ui_set_action_sensitive("convert_item", activate);
  gnac_ui_set_widget_sensitive("convert_button", activate);
}


void
gnac_bars_activate_file_list(gboolean activate)
{
  gnac_ui_set_widget_sensitive("file_list", activate);
}


void
gnac_bars_activate_pause(gboolean activate)
{
  gnac_ui_set_action_visible("pause_item", activate);
  gnac_ui_set_widget_sensitive("pause_button", activate);
}


void
gnac_bars_activate_preferences(gboolean activate)
{
  gnac_ui_set_action_sensitive("prefs_item", activate);
}


void
gnac_bars_activate_properties(gboolean activate)
{
  gnac_ui_set_action_sensitive("properties_item", activate);
}


void
gnac_bars_activate_quit(gboolean activate)
{
  gnac_ui_set_action_sensitive("quit_item", activate);
}


void
gnac_bars_activate_remove(gboolean activate)
{
  gnac_ui_set_action_sensitive("remove_item", activate);
}
