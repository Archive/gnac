/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *    
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#include "gnac-settings.h"
#include "libgnac-debug.h"


GSettings *settings;
GSettings *settings_ui;


static gboolean gnac_settings_set(GSettings   *settings,
                                  const gchar *key,
                                  GVariant    *value);

void
gnac_settings_init(void)
{
  settings = g_settings_new(GNAC_SCHEMA);
  settings_ui = g_settings_new(GNAC_SCHEMA_UI);
}


void
gnac_settings_bind(const gchar *key,
                   gpointer     object)
{
  g_settings_bind(settings, key, object, "active", G_SETTINGS_BIND_DEFAULT);
}


void
gnac_settings_ui_bind(const gchar *key,
                      gpointer     object)
{
  g_settings_bind(settings_ui, key, object, "active", G_SETTINGS_BIND_DEFAULT);
}


gboolean
gnac_settings_get_boolean(const gchar *key)
{
  return g_settings_get_boolean(settings, key);
}


gboolean
gnac_settings_ui_get_boolean(const gchar *key)
{
  return g_settings_get_boolean(settings_ui, key);
}


gboolean
gnac_settings_set_boolean(const gchar *key,
                          gboolean     value)
{
  return gnac_settings_set(settings, key, g_variant_new_boolean(value));
}


gboolean
gnac_settings_ui_set_boolean(const gchar *key,
                             gboolean     value)
{
  return gnac_settings_set(settings_ui, key, g_variant_new_boolean(value));
}


gint
gnac_settings_get_int(const gchar *key)
{
  return g_settings_get_int(settings, key);
}


gboolean
gnac_settings_set_int(const gchar *key,
                      gint         value)
{
  return gnac_settings_set(settings, key, g_variant_new_int32(value));
}


gchar *
gnac_settings_get_string(const gchar *key)
{
  return g_settings_get_string(settings, key);
}


gboolean
gnac_settings_set_string(const gchar *key,
                         const gchar *value)
{
  if (!value) return FALSE;
  return gnac_settings_set(settings, key, g_variant_new_string(value));
}


static gboolean
gnac_settings_set(GSettings   *settings,
                  const gchar *key,
                  GVariant    *value)
{
  gchar *val_str = g_variant_print(value, FALSE);
  gboolean ret = g_settings_set_value(settings, key, value);

  if (!ret) {
    libgnac_debug("Failed to set key \"%s\" to \"%s\"", key, val_str);
  } else {
    libgnac_debug("Key \"%s\" set to \"%s\"", key, val_str);
  }

  g_free(val_str);

  return ret;
}
