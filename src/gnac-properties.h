/*
 * This file is part of GNAC - Gnome Audio Converter
 *
 * Copyright (C) 2007 - 2012 Gnac
 *
 *    - DUPASQUIER  Benoit    <bdupasqu@src.gnome.org>
 *    - JOAQUIM     David     <djoaquim@src.gnome.org>
 *    - ROUX        Alexandre <alexroux@src.gnome.org>
 *
 * GNAC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GNAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNAC; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */

#ifndef GNAC_PROPERTIES_H
#define GNAC_PROPERTIES_H

#include <gio/gio.h>
#include <gtk/gtk.h>

#include "libgnac-metadata.h"

G_BEGIN_DECLS

enum {
  PROPERTY_NAME,
  PROPERTY_VALUE,
  PROPERTY_TOOLTIP,
  PROPERTY_VISIBLE,
  PROPERTY_COLS
};


void 
gnac_properties_window_show(void);

void 
gnac_properties_set_row(GtkTreeRowReference *current);

void 
gnac_properties_update_arrows(void);

void 
gnac_properties_on_back(GtkWidget *widget,
                        gpointer   data);

void 
gnac_properties_on_forward(GtkWidget *widget,
                           gpointer   data);

void 
gnac_properties_on_close(GtkWidget *widget,
                         gpointer   data);

gboolean
gnac_properties_on_delete_event(GtkWidget *widget,
                                GdkEvent  *event,
                                gpointer   user_data);

void
gnac_properties_destroy(void);

G_END_DECLS

#endif /* GNAC_PROPERTIES_H */
